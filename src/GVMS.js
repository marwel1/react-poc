import axios from "axios"
import { useState, useEffect } from "react"
import Eidr from "./gvms/Eidr"
import Standard from "./gvms/Standard"
import Empty from "./gvms/Empty"
import ATA from "./gvms/ATA"
import TIR from "./gvms/TIR"
import GmrList from "./gvms/GmrList"

const GVMS = () => {
    const [state, setstate] = useState(undefined)
    const [loadState, setLoadState] = useState()

    const runAuth = async () => {
        const path = 'https://api.contained.io/ens/token'
        const config = {
          headers: {
            "Content-Type": "application/json",
            "X-Api-Key": "bP0xX1Lgjm7lkz9b6z0om8OA0WCrOANq2Dscg946"
          },
          params: {
            "service": "cds",
            "user_id": "auth0|605346f080640d006f3a32e2"     
          }
        }
    
        const response = await axios.get(path, config)
        // console.log(response)
    
        if(response.data.no_token) {
          setLoadState(false)
          const client_id = "ArupJWFYyrB0rzgjuVIzdbAM8tw1"
                const scope = "write:customs-declaration write:customs-declarations-information"
                const hmrc_url = "test-api.service.hmrc.gov.uk"
                const redirect_uri = "https://bluering.contained.io/cds/search"
                window.location.href = 
                  `https://${hmrc_url}/oauth/authorize?client_id=${client_id}&scope=${scope}&response_type=code&redirect_uri=${redirect_uri}`
        } else {
          setLoadState(true)
          console.log("success")
        }
    }

    useEffect(() => {
        runAuth()
    }, [])

    return (
        <div>
            {
              loadState 
              ? <div>
                  <button onClick={()=>setstate(5)}>GMR LISTING</button>
                  <button onClick={()=>setstate(0)}>Standard</button>
                  <button onClick={()=>setstate(1)}>EIDR</button>
                  <button onClick={()=>setstate(2)}>EMPTY</button>
                  <button onClick={()=>setstate(3)}>ATA</button>
                  <button onClick={()=>setstate(4)}>TIR</button>
                </div>
              : <p>Verifying</p>
            }

            { state === 0 && <Standard/> }
            { state === 1 && <Eidr/> } 
            { state === 2 && <Empty/> } 
            { state === 3 && <ATA/> } 
            { state === 4 && <TIR/> }
            { state === 5 && <GmrList/> }
        </div>
    )
}

export default GVMS
