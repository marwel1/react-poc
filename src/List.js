import axios from "axios"
import { useEffect } from "react"

const List = () => {

    const listing = async () => {
        const gvmsHeaders = {
          headers: {
              Accept: 'application/vnd.hmrc.1.0+json',
              Authorization: 'Bearer 397807640de138c94d40f32c6868c5d8'
          }
        }

        //temporarily uses cors anywhere for cors bypass
        const response = await axios.get('https://cors-anywhere.herokuapp.com/https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements', gvmsHeaders)
        console.log('RESPONSE: ',response.data)
    }

    useEffect(() => {
        listing()
    }, [])

    return (
        <></>
    )
}

export default List
