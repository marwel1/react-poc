import { Switch, Route, BrowserRouter } from 'react-router-dom'
import GVMS from './GVMS'
import List from './List'

const App = () => {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path='/gvms' component={GVMS}  />
          <Route path='/gvms/list' component={List}  />
        </Switch>
    </BrowserRouter>
  )
}

export default App
