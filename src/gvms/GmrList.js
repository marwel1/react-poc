import axios from "axios"
import { useEffect, useState } from "react"

const GmrList = () => {
    const [list, setList] = useState()

    const listing = async () => {
        const gvmsHeaders = {
          headers: {
              Accept: 'application/vnd.hmrc.1.0+json',
              Authorization: 'Bearer 397807640de138c94d40f32c6868c5d8'
          }
        }
        //temporarily uses cors anywhere for cors bypass
        const response = await axios.get('https://cors-anywhere.herokuapp.com/https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements', gvmsHeaders)
        setList(response.data)
    }

    useEffect(() => {
        listing()
    }, [])
    
    return (
        <div>
            {
                list 
                ? list.map(data => 
                    <div key={data.gmrId} style={{margin : '2em'}}>
                      <p>gmrId: {data.gmrId}</p>
                      <p>direction: {data.direction}</p>
                      <p>state: {data.state}</p>
                      <p>createdDateTime: {data.createdDateTime}</p>
                      <p>updatedDateTime: {data.updatedDateTime}</p>
                      <p>isUnaccompanied: {String(data.isUnaccompanied)}</p>
                      <p>vehicleRegNum: {data.vehicleRegNum ? data.vehicleRegNum : 'not defined(for updating)'}</p>
                      <p>plannedCrossing:</p>
                      <ul>
                        <li>routeId: {data.plannedCrossing && data.plannedCrossing.routeId}</li>
                        <li>localDateTimeOfDeparture: {data.plannedCrossing && data.plannedCrossing.localDateTimeOfDeparture}</li>
                      </ul>
                      <hr/>
                    </div>
                )
                : <h2>Token is expired</h2>
            }
        </div>
    )
}

export default GmrList
