import { useState } from "react"

const Empty = () => {
    // VALUES TO SEND TO API
    const [empty, setEmpty] = useState({
        direction: undefined,
        isUnaccompanied: undefined,
        vehicleRegNum: undefined,
        trailerRegistrationNums: [],
        plannedCrossing: {
            routeId: undefined,
            localDateTimeOfDeparture: undefined
        },
        emptyVehicle: {
            isOwnVehicle: undefined,
            sAndSMasterRefNum: undefined
        },
    })

    return (
        <div style={{display: 'flex', flexDirection:'row', width:'40em', justifyContent:'space-between'}}>
            <div>
                <h3>EMPTY DECLARATION</h3>
                <form style={{display: 'flex', flexDirection:'column', width:'20em'}}>

                    Direction:
                    <select onChange={(e) => setEmpty({...empty, direction: e.target.value})}>
                        <option value="GB_TO_NI">GB_TO_NI</option>
                        <option value="NI_TO_GB">NI_TO_GB</option>
                        <option value="UK_INBOUND">UK_INBOUND</option>
                        <option value="UK_OUTBOUND">UK_OUTBOUND</option>
                    </select>

                    Unaccompanied:
                    <select onChange={(e) => setEmpty({...empty, isUnaccompanied: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>

                    Vehicle Registration Number:
                    <input
                        onChange={(e) => setEmpty({...empty, vehicleRegNum: e.target.value})}
                        placeholder='Vehicle Registration Number'
                    />

                    <br/>
                    Planned Crossing(Object):
                    <input
                        onChange={(e) => setEmpty({...empty, plannedCrossing: {...empty.plannedCrossing, routeId: e.target.value} })}
                        placeholder='Route ID'
                    />
                    <input
                        onChange={(e) => setEmpty({...empty, plannedCrossing: {...empty.plannedCrossing, localDateTimeOfDeparture: e.target.value} })}
                        placeholder='D & T Local Departure'
                    />

                    <br/>
                    Empty Vehicle(Object):
                    <select onChange={(e) => setEmpty({...empty, emptyVehicle: {...empty.emptyVehicle, isOwnVehicle: e.target.value} })}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>
                    <input
                        onChange={(e) => setEmpty({...empty, emptyVehicle: {...empty.emptyVehicle, sAndSMasterRefNum: e.target.value} })}
                        placeholder='MRN'
                    />

                    <br/>
                    <button type="submit"> Submit </button>
                </form>
            </div>
            

            <div>
                <h3>Form Values</h3>
                <p>Direction: {empty.direction}</p>
                <p>Unaccompanied: {empty.isUnaccompanied}</p>
                <p>Vehicle Registration Number: {empty.vehicleRegNum}</p>
                <hr/>
                <p>Planned Crossing:</p>
                    <p>- Route ID: {empty.plannedCrossing.routeId}</p>
                    <p>- Local D & T of Departure: {empty.plannedCrossing.localDateTimeOfDeparture}</p>
                <hr/>
                <p>Empty Declarations:</p>
                    <p>- Empty Vehicle: {empty.emptyVehicle.isOwnVehicle}</p>
                    <p>- MRN: {empty.emptyVehicle.sAndSMasterRefNum}</p>
                <hr/>
            </div>
        </div>
    )
}

export default Empty
