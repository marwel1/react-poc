import { useState } from "react"

const ATA = () => {
    // VALUES TO SEND TO API
    const [ata, setAta] = useState({
        direction: undefined,
        isUnaccompanied: undefined,
        containerReferenceNums: [],
        plannedCrossing: {
            routeId: undefined,
            localDateTimeOfDeparture: undefined
        },
        ataDeclarations: [],
    })
    const [ad, setAd] = useState()
    const [container, setContainer] = useState()

    const addata = (e) => {
        setAta({...ata, ataDeclarations: [...ata.ataDeclarations, ad]})
        e.preventDefault()
    }
    const addContainer = (e) => {
        setAta({...ata, containerReferenceNums: [...ata.containerReferenceNums, container]})
        e.preventDefault()
    }
    return (
        <div style={{display: 'flex', flexDirection:'row', width:'40em', justifyContent:'space-between'}}>
            <div>
                <h3>ATA DECLARATION</h3>
                <form style={{display: 'flex', flexDirection:'column', width:'20em'}}>

                    Direction:
                    <select onChange={(e) => setAta({...ata, direction: e.target.value})}>
                        <option value="GB_TO_NI">GB_TO_NI</option>
                        <option value="NI_TO_GB">NI_TO_GB</option>
                        <option value="UK_INBOUND">UK_INBOUND</option>
                        <option value="UK_OUTBOUND">UK_OUTBOUND</option>
                    </select>

                    Unaccompanied:
                    <select onChange={(e) => setAta({...ata, isUnaccompanied: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>

                    Container Reference Numbers(array):
                    <input
                        onChange={(e) => setContainer(e.target.value)}
                        placeholder='Container Reference Numbers'
                    />
                    <button onClick={addContainer}>add Container Numbers</button>


                    <br/>
                    Planned Crossing(Object):
                    <input
                        onChange={(e) => setAta({...ata, plannedCrossing: {...ata.plannedCrossing, routeId: e.target.value} })}
                        placeholder='Route ID'
                    />
                    <input
                        onChange={(e) => setAta({...ata, plannedCrossing: {...ata.plannedCrossing, localDateTimeOfDeparture: e.target.value} })}
                        placeholder='D & T Local Departure'
                    />

                    <br/>
                    ATA Declarations(Array of Objects):
                    <input
                        onChange={(e) => setAd({...ata, traderEORI: e.target.value})}
                        placeholder='Trader EORI'
                    />
                    <button onClick={addata}>add ata</button>

                    <br/>
                    <button type="submit"> Submit </button>
                </form>
            </div>
            

            <div>
                <h3>Form Values</h3>
                <p>Direction: {ata.direction}</p>
                <p>Unaccompanied: {ata.isUnaccompanied}</p>
                <p>Container Registration Numbers: {ata.containerReferenceNums.map(data => <p>{data}</p> )}</p>
                <hr/>
                <p>Planned Crossing:</p>
                <p>- Route ID: {ata.plannedCrossing.routeId}</p>
                <p>- Local D & T of Departure: {ata.plannedCrossing.localDateTimeOfDeparture}</p>
                <hr/>
                <p>ata Declarations:</p>
                {
                    ata.ataDeclarations.map(data => <p>- ata Declaration ID: {data.traderEORI}</p>)
                }
                <hr/>
            </div>
        </div>
    )
}

export default ATA
