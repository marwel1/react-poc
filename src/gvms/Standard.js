import { useState } from "react"

const Standard = () => {
    // VALUES TO SEND TO API
    const [standard, setStandard] = useState({
        direction: undefined,
        isUnaccompanied: undefined,
        vehicleRegNum: undefined,
        plannedCrossing: {
            routeId: undefined,
            localDateTimeOfDeparture: undefined
        },
        customsDeclarations: [],
        transitDeclarations: [],
    })
    const [cd, setCd] = useState()
    const [td, setTd] = useState()

    const addCd = (e) => {
        setStandard({...standard, customsDeclarations: [...standard.customsDeclarations, cd]})
        e.preventDefault()
    }
    const addTd = (e) => {
        setStandard({...standard, transitDeclarations: [...standard.transitDeclarations, td]})
        e.preventDefault()
    }

    return (
        <div style={{display: 'flex', flexDirection:'row', width:'40em', justifyContent:'space-between'}}>
            <div>
                <h3>STANDARD DECLARATION</h3>
                <form style={{display: 'flex', flexDirection:'column', width:'20em'}}>

                    Direction:
                    <select onChange={(e) => setStandard({...standard, direction: e.target.value})}>
                        <option value="GB_TO_NI">GB_TO_NI</option>
                        <option value="NI_TO_GB">NI_TO_GB</option>
                        <option value="UK_INBOUND">UK_INBOUND</option>
                        <option value="UK_OUTBOUND">UK_OUTBOUND</option>
                    </select>

                    Unaccompanied:
                    <select onChange={(e) => setStandard({...standard, isUnaccompanied: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>

                    Vehicle Registration Number:
                    <input
                        onChange={(e) => setStandard({...standard, vehicleRegNum: e.target.value})}
                        placeholder='Vehicle Registration Number'
                        
                    />

                    <br/>
                    Planned Crossing(Object):
                    <input
                        onChange={(e) => setStandard({...standard, plannedCrossing: {...standard.plannedCrossing, routeId: e.target.value} })}
                        placeholder='Route ID'
                    />
                    <input
                        onChange={(e) => setStandard({...standard, plannedCrossing: {...standard.plannedCrossing, localDateTimeOfDeparture: e.target.value} })}
                        placeholder='D & T Local Departure'
                    />

                    <br/>
                    Custom Declarations(Array of Objects):
                    <input
                        onChange={(e) => setCd({...cd, customsDeclarationId: e.target.value})}
                        placeholder='Customs Declarations ID'
                    />

                    <input
                        onChange={(e) => setCd({...cd, sAndSMasterRefNum: e.target.value})}
                        placeholder='MRN'
                    />
                    <button onClick={addCd}>add CD</button>

                    <br/>
                    Transit Declarations(Array of Objects):
                    <input
                        onChange={(e) => setTd({...td, transitDeclarationId: e.target.value})}
                        placeholder='Transit Declarations ID'
                    />
                    <input
                        onChange={(e) => setTd({...td, sAndSMasterRefNum: e.target.value})}
                        placeholder='MRN'
                    />
                    TSAD:
                    <select onChange={(e) => setTd({...td, isTSAD: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>
                    <button onClick={addTd}>add TD</button>

                    <br/>
                    <button type="submit"> Submit </button>
                </form>
            </div>
            

            <div>
                <h3>Form Values</h3>
                <p>Direction: {standard.direction}</p>
                <p>Unaccompanied: {standard.isUnaccompanied}</p>
                <p>Vehicle Registration Number: {standard.vehicleRegNum}</p>
                <hr/>
                <p>Planned Crossing:</p>
                <p>- Route ID: {standard.plannedCrossing.routeId}</p>
                <p>- Local D & T of Departure: {standard.plannedCrossing.localDateTimeOfDeparture}</p>
                <hr/>
                <p>Custom Declarations:</p>
                {
                    standard.customsDeclarations.map(data => 
                        <div>
                            <p>- Customs Declaration ID: {data.customsDeclarationId}</p>
                            <p>- MRN: {data.sAndSMasterRefNum}</p>
                        </div>
                    )
                }
                <hr/>
                <p>Transit Declarations:</p>
                {
                    standard.transitDeclarations.map(data => 
                        <div>
                            <p>- Transit Declaration ID: {data.transitDeclarationId} </p>
                            <p>- MRN: {data.sAndSMasterRefNum}</p>
                            <p>- TSAD: {data.isTSAD}</p>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default Standard
