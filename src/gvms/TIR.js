import { useState } from "react"

const TIR = () => {
    // VALUES TO SEND TO API
    const [tir, setTir] = useState({
        direction: undefined,
        isUnaccompanied: undefined,
        trailerRegistrationNums: [],
        plannedCrossing: {
            routeId: undefined,
            localDateTimeOfDeparture: undefined
        },
        sAndSMasterRefNum: undefined,
        tirDeclarations: [],
    })


    const [td, setTd] = useState()
    const [trailer, setTrailer] = useState()

    const addTir = (e) => {
        setTir({...tir, tirDeclarations: [...tir.tirDeclarations, td]})
        e.preventDefault()
    }
    const addTrailer = (e) => {
        setTir({...tir, trailerRegistrationNums: [...tir.trailerRegistrationNums, trailer]})
        e.preventDefault()
    }
    return (
        <div style={{display: 'flex', flexDirection:'row', width:'40em', justifyContent:'space-between'}}>
            <div>
                <h3>TIR DECLARATION</h3>
                <form style={{display: 'flex', flexDirection:'column', width:'20em'}}>

                    Direction:
                    <select onChange={(e) => setTir({...tir, direction: e.target.value})}>
                        <option value="GB_TO_NI">GB_TO_NI</option>
                        <option value="NI_TO_GB">NI_TO_GB</option>
                        <option value="UK_INBOUND">UK_INBOUND</option>
                        <option value="UK_OUTBOUND">UK_OUTBOUND</option>
                    </select>

                    Unaccompanied:
                    <select onChange={(e) => setTir({...tir, isUnaccompanied: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>

                    Trailer Registration Numbers(array):
                    <input
                        onChange={(e) => setTrailer(e.target.value)}
                        placeholder='Container Reference Numbers'
                    />
                    <button onClick={addTrailer}>add Trailer Numbers</button>


                    <br/>
                    Planned Crossing(Object):
                    <input
                        onChange={(e) => setTir({...tir, plannedCrossing: {...tir.plannedCrossing, routeId: e.target.value} })}
                        placeholder='Route ID'
                    />
                    <input
                        onChange={(e) => setTir({...tir, plannedCrossing: {...tir.plannedCrossing, localDateTimeOfDeparture: e.target.value} })}
                        placeholder='D & T Local Departure'
                    />
                    
                    <br/>
                    MRN:
                    <input
                        onChange={(e) => setTir({...tir, sAndSMasterRefNum: e.target.value })}
                        placeholder='Route ID'
                    />

                    <br/>
                    tir Declarations(Array of Objects):
                    <input
                        onChange={(e) => setTd({...tir, tirCarnetId: e.target.value})}
                        placeholder='Trader EORI'
                    />
                    <button onClick={addTir}>add TIR</button>

                    <br/>
                    <button type="submit"> Submit </button>
                </form>
            </div>
            

            <div>
                <h3>Form Values</h3>
                <p>Direction: {tir.direction}</p>
                <p>Unaccompanied: {tir.isUnaccompanied}</p>
                <p>Trailer Registration Numbers: {tir.trailerRegistrationNums.map(data => <p>{data}</p> )}</p>
                <hr/>
                <p>Planned Crossing:</p>
                <p>- Route ID: {tir.plannedCrossing.routeId}</p>
                <p>- Local D & T of Departure: {tir.plannedCrossing.localDateTimeOfDeparture}</p>
                <hr/>

                <p>MRN: {tir.sAndSMasterRefNum}</p>
                <p>TIR Declarations:</p>
                {
                    tir.tirDeclarations.map(data => <p>- tir Declaration ID: {data.tirCarnetId}</p>)
                }
                <hr/>
            </div>
        </div>
    )
}

export default TIR
