import { useState } from "react"

const Eidr = () => {
    // VALUES TO SEND TO API
    const [eidr, setEidr] = useState({
        direction: undefined,
        isUnaccompanied: undefined,
        vehicleRegNum: undefined,
        trailerRegistrationNums: [],
        plannedCrossing: {
            routeId: undefined,
            localDateTimeOfDeparture: undefined
        },
        eidrDeclarations: [],
    })
    const [tr, setTr] = useState()
    const [ed, setEd] = useState()

    const addTrailer = (e) => {
        setEidr({...eidr, trailerRegistrationNums: [...eidr.trailerRegistrationNums, tr]})
        e.preventDefault()
    }
    const addEidr = (e) => {
        setEidr({...eidr, eidrDeclarations: [...eidr.eidrDeclarations, ed]})
        e.preventDefault()
    }

    return (
        <div style={{display: 'flex', flexDirection:'row', width:'40em', justifyContent:'space-between'}}>
            <div>
                <h3>EIDR DECLARATION</h3>
                <form style={{display: 'flex', flexDirection:'column', width:'20em'}}>

                    Direction:
                    <select onChange={(e) => setEidr({...eidr, direction: e.target.value})}>
                        <option value="GB_TO_NI">GB_TO_NI</option>
                        <option value="NI_TO_GB">NI_TO_GB</option>
                        <option value="UK_INBOUND">UK_INBOUND</option>
                        <option value="UK_OUTBOUND">UK_OUTBOUND</option>
                    </select>

                    Unaccompanied:
                    <select onChange={(e) => setEidr({...eidr, isUnaccompanied: e.target.value})}>
                        <option value="true">True</option>
                        <option value="false">False</option>
                    </select>

                    Vehicle Registration Number:
                    <input
                        onChange={(e) => setEidr({...eidr, vehicleRegNum: e.target.value})}
                        placeholder='Vehicle Registration Number'
                    />

                    Trailer Registration Numbers(Array):
                    <input
                        onChange={(e) => setTr(e.target.value)}
                        placeholder='Trail Registration Numbers'
                    />
                    <button onClick={addTrailer}>add Trailer Numbers</button>


                    <br/>
                    Planned Crossing(Object):
                    <input
                        onChange={(e) => setEidr({...eidr, plannedCrossing: {...eidr.plannedCrossing, routeId: e.target.value} })}
                        placeholder='Route ID'
                    />
                    <input
                        onChange={(e) => setEidr({...eidr, plannedCrossing: {...eidr.plannedCrossing, localDateTimeOfDeparture: e.target.value} })}
                        placeholder='D & T Local Departure'
                    />

                    <br/>
                    EIDR Declarations(Array of Objects):
                    <input
                        onChange={(e) => setEd({...eidr, traderEORI: e.target.value})}
                        placeholder='Trader EORI'
                    />
                    <input
                        onChange={(e) => setEd({...eidr, sAndSMasterRefNum: e.target.value})}
                        placeholder='MRN'
                    />
                    <button onClick={addEidr}>add EIDR</button>

                    <br/>
                    <button type="submit"> Submit </button>
                </form>
            </div>
            

            <div>
                <h3>Form Values</h3>
                <p>Direction: {eidr.direction}</p>
                <p>Unaccompanied: {eidr.isUnaccompanied}</p>
                <p>Vehicle Registration Number: {eidr.vehicleRegNum}</p>
                <p>Trailer Registration Numbers: {eidr.trailerRegistrationNums.map(data => <p>{data}</p> )}</p>
                <hr/>
                <p>Planned Crossing:</p>
                <p>- Route ID: {eidr.plannedCrossing.routeId}</p>
                <p>- Local D & T of Departure: {eidr.plannedCrossing.localDateTimeOfDeparture}</p>
                <hr/>
                <p>EIDR Declarations:</p>
                {
                    eidr.eidrDeclarations.map(data => 
                        <div>
                            <p>- EIDR Declaration ID: {data.customsDeclarationId}</p>
                            <p>- MRN: {data.sAndSMasterRefNum}</p>
                        </div>
                    )
                }
                <hr/>
            </div>
        </div>
    )
}

export default Eidr
